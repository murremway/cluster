variable "cluster-name" {
  default = "particle"
  type    = string
}

variable "creator" {
  default = "particle41"
  type    = string
}

variable "instance_type" {
  default = "t3a.large"
  type    = string
}

variable "keypair-name" {
  default = ""
  type    = string
}

variable "AWS_REGION" {
  default = "us-east-1"
}

data "aws_ami" "amazon_linux_2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "block-device-mapping.volume-type"
    values = ["gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}