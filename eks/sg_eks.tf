#SG to control access to worker nodes
resource "aws_security_group" "eks-master-sg" {
    name        = "particle41"
    description = "Cluster communication with worker nodes"
    vpc_id      = module.vpc.vpc_id

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["10.0.0.0/16"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
  }
 
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
  }


}

resource "aws_security_group" "eks-node-sg" {
        name        = "forty-one"
        description = "Security group for all nodes in the cluster"
        vpc_id      = module.vpc.vpc_id
        

        egress {
            from_port   = 0
            to_port     = 0
            protocol    = "-1"
            cidr_blocks = ["10.0.0.0/16"]
        }

        ingress {
            from_port = 80
            to_port = 80
            protocol = "tcp"
            cidr_blocks = ["10.0.0.0/16"]
  }
 
        ingress {
            from_port = 22
            to_port = 22
            protocol = "tcp"
            cidr_blocks = ["10.0.0.0/16"]
  }
}


resource "aws_security_group" "worker_ssh" {
  name_prefix = "worker_ssh"
  vpc_id      = module.vpc.vpc_id
  
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/16"]
  }
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }
}
