What resources are created

VPC

Internet Gateway (IGW)

Public and Private Subnets

Security Groups, Route Tables and Route Table Associations

IAM roles, instance profiles and policies

An EKS Cluster

EKS Managed Node group

Autoscaling group and Launch Configuration

Worker Nodes in a private Subnet

The Aws Region is set to us-east-1, This can be changed to the required Region in variable.tf also go through the variables and make sure to check and specify the key-pair name inside the variable

Also a provision to backup state file into S3 bucked is specified all you need to do is uncomment this with ctlr+k+u if you are using vscode and specify all the necssary parameters for S3 bucket


IF the terraform apply fails due to configmap and aws-auth rerun apply again and when all resources are being created pls ssh into all the instances and switch to root with sudo su and run the bootstrap script with this command 

cd /etc/eks/.bootstrap.sh --apiserver-endpoint '(this can be foundd in the aws eks console api-endpoint)' --b64-cluster-ca '(paste cluster certifcate)'