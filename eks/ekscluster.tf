locals {
  common_tags = {
 creator      = var.creator 
 }
 worker_name  = "forty-one"
 cluster_name = "particle"
} 


resource "aws_eks_cluster" "aws_eks" {
  enabled_cluster_log_types = ["authenticator","api"]
  name     = "particle"
  role_arn = aws_iam_role.eks-cluster.arn

  vpc_config {
    subnet_ids = module.vpc.private_subnets
    security_group_ids = [aws_security_group.eks-master-sg.id]
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,  
    aws_iam_role_policy_attachment.AmazonEKSServicePolicy,
    
  ]

  tags = {
    Name = "particle"
  }
}

########################################################################################
# Setup AutoScaling Group for worker nodes
########################################################################################


resource "aws_launch_configuration" "config" {
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.eks_worker.name
  image_id                    = data.aws_ami.eks-worker.id
  instance_type               = var.instance_type
  security_groups             = [aws_security_group.eks-node-sg.id, aws_security_group.worker_ssh.id]
  user_data_base64            = base64encode(local.aws-eks-node-userdata)
  key_name                    = var.keypair-name

  lifecycle {
    create_before_destroy = true
  }
  ebs_optimized           = true
  root_block_device {
    volume_size           = 100
    delete_on_termination = true
  }
}

resource "aws_autoscaling_group" "asg" {
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.config.id
  max_size             = 2
  min_size             = 2
  name                 = local.worker_name
  vpc_zone_identifier  = module.vpc.private_subnets

  tag {
    key                 = "Name"
    value               = local.worker_name
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${aws_eks_cluster.aws_eks.name}"
    value               = "owned"
    propagate_at_launch = true
  }
}