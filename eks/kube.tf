data "aws_eks_cluster_auth" "aws_iam_authenticator" {
  name = "${aws_eks_cluster.aws_eks.name}"
}

locals {
  # roles to allow kubernetes access via cli and allow ec2 nodes to join eks cluster
  configmap_roles = [{
    rolearn  ="${aws_iam_role.eks_nodes.arn}"
    username = "{{SessionName}}"
    groups   = ["system:masters"]
  },
  {
    rolearn  = "${aws_iam_role.eks-cluster.arn}"
    username = "system:node:{{EC2PrivateDNSName}}"
    groups   = ["system:bootstrappers","system:nodes"]
  },
    {
    rolearn  = "${aws_iam_role.eks_nodes.arn}"
    username = "{{SessionName}}"
    groups   = ["system:masters"]
  },]
}

# Allow worker nodes to join cluster via config map

resource "kubernetes_config_map" "aws_auth" {
  metadata {
    name = "aws-auth"
    namespace = "kube-system"
  }
 data = {
    mapRoles = yamlencode(local.configmap_roles)
  }
}

# generate KUBECONFIG as output to save in ~/.kube/config locally
# save the 'terraform output eks_kubeconfig > config', run 'mv config ~/.kube/config' to use it for kubectl
locals {
  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters: 
- cluster:  
    server: ${aws_eks_cluster.aws_eks.endpoint}
    certificate-authority-data: ${aws_eks_cluster.aws_eks.certificate_authority.0.data}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${aws_eks_cluster.aws_eks.name}"
KUBECONFIG
}

output "kubeconfig" {
  value = "${local.kubeconfig}"
}

locals {
  aws-eks-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.aws_eks.endpoint}' --b64-cluster-ca '${aws_eks_cluster.aws_eks.certificate_authority.0.data}'
USERDATA
}